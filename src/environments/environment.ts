// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCu1uzA5PrctN8pn4RR936TyK2euuN0bU0",
    authDomain: "organizador-358a4.firebaseapp.com",
    projectId: "organizador-358a4",
    storageBucket: "organizador-358a4.appspot.com",
    messagingSenderId: "166162783478",
    appId: "1:166162783478:web:8f831e399bb549652d20d7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
