import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Item } from 'src/app/models/item.interface';

@Component({
  selector: 'app-items-detail',
  templateUrl: './items-detail.component.html',
  styleUrls: ['./items-detail.component.scss']
})
export class ItemsDetailComponent implements OnInit {

  idItems: string = '';
  itemSelected: Item | undefined;

  constructor(private firestoreStoreService: FirestoreService,private activatedRoute: ActivatedRoute, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      if (params.id) {
        this.idItems = params.id
      } else {
        alert('No Item found');
        this.returnBack();
      }
    });

    this.firestoreStoreService.obtenerItem(this.idItems).subscribe(i => {
      this.itemSelected = i;
      console.log(this.itemSelected);
    });


  }

  returnBack() {
    // 1.
    // this.router.navigate(['/contacts']);
    // 2.
    this.location.back();
  }
}
