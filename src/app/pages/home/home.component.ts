import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { addDays, startOfDay, subDays } from 'date-fns';
import { Timestamp } from 'rxjs/internal/operators/timestamp';
import { Item } from 'src/app/models/item.interface';
import { Market } from 'src/app/models/market.interface';
import { FireStoreAuthService } from 'src/app/services/firestore-auth.service';
import { FirestoreService } from 'src/app/services/firestore.service';
import {
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  endOfDay,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CalendarEventAction, CalendarEventTimesChangedEvent} from 'angular-calendar';
import { Compra } from 'src/app/models/compra.interface';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /** VARIABLES PARA CALENDARIO **/
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any> | undefined;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
      },
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  /** VARIABLES PARA SERVICES **/

  listaCompras: Compra[] = [];

  constructor(private modal: NgbModal, private firestoreStoreService: FirestoreService, public authService: FireStoreAuthService) {

  }

  ngOnInit(): void {

    this.obtenerCompras();

    this.obtenerCalendarioCompras();

  }

  private obtenerCalendarioCompras() {
    for (let i = 0; i < this.listaCompras.length; i++) {
      const element = this.listaCompras[i];
      var obj = {
        start: subDays(startOfDay(element.fecha), 0),
        end: addDays(element.fecha, 0),
        title: 'Compra',
        color: colors.red,
        actions: this.actions,
        allDay: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
        draggable: true,
      };
    }
    this.events.push();
  }


  private obtenerCompras() {
    this.firestoreStoreService.obtenerTodasLasCompras().subscribe((compras: Compra[]) => {
      this.listaCompras = compras;
      return this.listaCompras;
    });
  }

  crearItem() {
    let nuevoItem: Item = {
      name: "Item 1",
      image: "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA03/201903/28/00120904500020____2__600x600.jpg",
      lastBuy: new Date(),
      market: "ULei9KCqPFzUh5X9KzDM",
      price: 2.35,
      quantity: 0
    }

    this.firestoreStoreService.crearItem(nuevoItem).then((response) => {
      console.log(`Item creado correctamente`);
      console.table(response);
    }).catch((err) => {

      console.log(`Error al crear el nuevo item: ${err}`);
    });
  }

  borrarItem(id: string) {
    this.firestoreStoreService.borrarItem(id).then(_ => {
      console.log(`Item con id ${id} eliminada correctamente`);
    }).catch((err) => {
      console.log(`Error al eliminar item con id ${id}: ${err}`);
    })

  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

}
