import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/models/item.interface';
import { Market } from 'src/app/models/market.interface';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  listadoMarkets: Market[] = [];
  listadoItems: Item[] = [];


  constructor(private firestoreStoreService: FirestoreService) { }

  ngOnInit(): void {


    this.obtenerItems();
  }


  private obtenerItems() {
    this.firestoreStoreService.obtenerTodosLosItems().subscribe((items: Item[]) => {
      this.listadoItems = items;
    });
  }

}
