import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';

import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Compra } from '../models/compra.interface';

import { Item } from '../models/item.interface';
import { Market } from '../models/market.interface';
import { UserData } from '../models/user-data.interface';
import { User } from '../models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {


  private usersCollection: AngularFirestoreCollection<UserData>;
  private itemsCollection: AngularFirestoreCollection<Item>;
  private marketsCollection: AngularFirestoreCollection<Market>;
  private compraCollection: AngularFirestoreCollection<Compra>;

  private listaUsers: Observable<UserData[]>;
  private listaItems: Observable<Item[]>;
  private listaMarkets: Observable<Market[]>;
  private listaCompra: Observable<Compra[]>;

  constructor(private firestore: AngularFirestore) {

    this.compraCollection = this.firestore.collection<Compra>('compra');
    this.listaCompra = this.compraCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          return {
            id: id,
            ...datos
          }
        });
      })
    )

    this.usersCollection = this.firestore.collection<UserData>('user');
    this.listaUsers = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          return {
            id: id,
            ...datos
          }
        });
      })
    )

    this.marketsCollection = this.firestore.collection<Market>('markets');
    this.listaMarkets = this.marketsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          return {
            id: id,
            ...datos
          }
        });
      })
    )


    this.itemsCollection = this.firestore.collection<Item>('items');
    this.listaItems = this.itemsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          return {
            id: id,
            ...datos
          }
        });
      })
    )

   }


  /****************************** COMPRA ******************************/

  obtenerTodasLasCompras(): Observable<Compra[]>{

    return this.listaCompra;
  }

  /************* ITEMS **********/
  /**
   *Metodo que se conecta con la coleccion"Items"
   * y devuelve la lista de la mismas
   */
  obtenerTodosLosItems(): Observable<Item[]>{
    return this.listaItems;
  }

  obtenerItem(id: string): Observable<Item | undefined> {
    return this.itemsCollection.doc<Item>(id).valueChanges();
  }

  actualizarItem(itemActualizada: Item, id: string): Promise<void>{
    return this.itemsCollection.doc<Item>(id).update(itemActualizada);
  }

  crearItem(itemNuevo: Item): Promise<DocumentReference<Item>> {
    return this.itemsCollection.add(itemNuevo);
  }

  borrarItem(id: string): Promise<void>{
    return this.itemsCollection.doc<Item>(id).delete();
  }

  /************* MARKETS **********/

   /**
   *Metodo que se conecta con la coleccion"Items"
   * y devuelve la lista de la mismas
   */
  obtenerTodosLosMarkets(): Observable<Market[]>{
    return this.listaMarkets;
  }




  /******************* USERS ************ */

  obtenerDatosUsuarioByUid(uid:string): Observable<UserData|undefined>{
    return this.usersCollection.doc<UserData>(uid).valueChanges();
  }


}
