import { Component } from '@angular/core';
import { FireStoreAuthService } from './services/firestore-auth.service';
import firebase from 'firebase';
import { FirestoreService } from './services/firestore.service';
import { UserData } from './models/user-data.interface';
import { User } from './models/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pantry';

  userLoged: firebase.User | null = null;
  userDataLoged: UserData | undefined;

  constructor(public authService: FireStoreAuthService, public firestoreService: FirestoreService) { }

  ngOnInit(): void {
    // this.getUserLogedData();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // this.getUserLogedData();
  }


  // private getUserLogedData() {
  //   var userJson = localStorage.getItem('user');
  //   console.log(userJson);
  //   if (userJson) {
  //     this.userLoged = JSON.parse(userJson);
  //     console.log(this.userLoged?.displayName);
  //     var uid = this.userLoged?.uid;
  //     if (uid) {
  //       this.firestoreService.obtenerDatosUsuarioByUid(uid).subscribe(i => {
  //         this.userDataLoged = i;
  //       });
  //     }
  //   } else {
  //     console.error('No hay usuario logueado');
  //   }
  // }
}

