export interface ItemsCompra {
  cantidad: number,
  item: string,
  supermercado: string,
  precio: number
}
