export interface Market {
  id?: string,
  logo: string;
  name: string;
  place: string;

}
