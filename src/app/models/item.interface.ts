import { Timestamp } from "rxjs";

export interface Item {
  id?: string,
  image: string;
  lastBuy: Date;
  market: string;
  name: string;
  price: number;
  quantity: number;

}
