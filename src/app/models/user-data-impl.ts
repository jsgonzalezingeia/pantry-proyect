import { UserData } from "./user-data.interface";

export class UserDataImpl implements UserData{
  id?: string | undefined;
  nombre: string;
  apellidos: string;
  image: string;
  email: string;

  constructor() {
    this.id = '';
    this.nombre = '';
    this.apellidos = '';
    this.image = '';
    this.email = '';
  }


}
