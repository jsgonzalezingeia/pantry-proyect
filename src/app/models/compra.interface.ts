import { ItemsCompra } from "./items-compra";

export interface Compra {
  fecha: Date;
  listaCompra: ItemsCompra[]
}
